import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {GoogleAnalyticsService} from "../../services/googleAnalytics.service";

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Input() show: boolean = false;

  @Input() modalType: string = ''; // flop, turn or river

  @Input() modalMode: string = ''; // me or other

  @Input() stats: any;

  @Output() close: EventEmitter<void> = new EventEmitter<void>();

  handsIndex = 0;

  data = [];

  constructor(private googleAnalyticsService: GoogleAnalyticsService) { }

  ngOnInit(): void {
  }

  back() {
    if (!this.isFirst()) {
      this.handsIndex--;
    }
  }

  getHand(hand: string) {
    return (hand.match(/.{1,2}/g) || []).join(' ');
  }

  forward() {
    if (!this.isLast()) {
      this.handsIndex++;
    }
  }

  getClassName(str: string) {
    return str.toLowerCase().split(' ').join('-');
  }

  isFirst(){
    return this.handsIndex === 0;
  }

  isLast() {
    return this.handsIndex === this.stats.betterHandsList.length - 1;
  }

  toggle(detail: any) {
    detail.open = !detail.open;
  }

  setIndex(index: number) {
    this.handsIndex = index;
  }

  setModalMode(mode: string) {
    this.modalMode = mode;
    if (this.modalMode === 'me') {
      this.googleAnalyticsService.addStatEvent('show_possible_hands_for_me', '', '', 'click', 1);
    } else {
      this.googleAnalyticsService.addStatEvent('show_possible_better_hands_for_others', '', '', 'click', 1);
    }
  }

}
