import { Component } from '@angular/core';
import {WebsocketService} from "./services/websocket.service";
import {NavigationEnd, Router} from "@angular/router";

declare let gtag: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private websocketService: WebsocketService, private router: Router) {
    this.websocketService.connect();

    this.router.events.subscribe(event => {
      if(event instanceof NavigationEnd){
        gtag('config', 'G-LRFG37WPRX',
          {
            'page_path': event.urlAfterRedirects
          }
        );
      }
    })
  }

}
